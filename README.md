# Домашнее задание №1. Scoring API
Курс "Python Developer. Professional"

## Сервис
Сервис представляет собой HTTP-сервер, реализующий API скоринга. 
Сервис принимает HTTP POST-запросы с параметрами в формате JSON 
и возвращает либо результаты выполнения запроса, либо сообщение об ошибке.

Запуск сервера: `$ python api.py [--port HTTP_PORT] [--log PATH/TO/LOG/FILE]`

Завершение сервера: по сигналу с клавиатуры

Предполагаемая версия интерпретатора: `3.9`

Параметры командной строки:
- `--port`: HTTP-порт, который слушает сервер, по умолчанию 8080
- `--log`: путь до файла логов сервера, по умолчанию логи пишутся в `stderr`

Варианты ответа сервера:
- при успешном выполнении запроса возвращает `200 OK` и JSON с результатами выполнения запроса
- при ошибке парсинга запроса как JSON возвращает `400 Bad Request`
- при ошибке валидации запроса возвращает `422 Invalid Request` и описание ошибки валидации в формате plain text
- при ошибке авторизации возвращает `403 Forbidden`
- при попытке вызова несуществующего метода (отличного от `online_score` или `clients_interests`) возвращает `404 Not Found`

## Тесты
Запуск: `$ python -m unittest -v test.py`
